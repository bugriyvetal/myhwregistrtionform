package com.company.me.view;

import java.util.Locale;
import java.util.ResourceBundle;

public class View {
    static String MESSAGES_BUNDLE_NAME = "messages";
    public static final ResourceBundle bundle =
            ResourceBundle.getBundle(
                    MESSAGES_BUNDLE_NAME,
                    new Locale("ukr"));


public void print_message (String message) {
    System.out.println(message);
}


public String stringConcat (String ... messages) {

    StringBuilder sb = new StringBuilder();
    for (String a: messages) {
        sb.append(a);
    }

    return new String(sb);
}

public void input(String message) {
print_message(stringConcat(bundle.getString(TEXT_CONSTANTS.INPUT_STRING_DATA),message

));
}

public void wrongInput(String message) {
    print_message(stringConcat(bundle.getString(TEXT_CONSTANTS.WRONG_INPUT_DATA), message));
}



}
