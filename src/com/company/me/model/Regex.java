package com.company.me.model;

public interface Regex {
    String FName = "^[А-ЩЬЮЯҐІЇЄ][а-щьюяґіїє']{1,20}$";
    String FNAME_lat = "^[A-Z][a-z]{1,20}$";
    String login= "^[A-Za-z0-9_-]{8,20}$";
}
